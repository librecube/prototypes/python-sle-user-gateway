# SLE User Gateway

The SLE User Gateway provides a link between the ground stations and mission
control systems using the Space Link Extension (SLE) services.

To be written...

The Manager models the state of the service instances and handles requests
from external users, such as a GUI application.

The Servers (Raf, Rcf, etc.) provide the interface between ground station
and the mission control system.

The User components are adapters for different mission control systems.
