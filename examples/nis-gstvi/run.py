from sle_user_gateway import GatewayManager
from sle_user_gateway.user_interface.nis import NisInterface

import logging
logging.basicConfig(level=logging.DEBUG)


# define the user side interface
user_interface = NisInterface(version=0)
user_interface.process_channel_file("channels.yml")
user_interface.startup()

# create gateway and read config and sicf files
gateway_manager = GatewayManager()
gateway_manager.load_user_interface(user_interface)
gateway_manager.process_proxy_config_file("proxy_config.yml")
gateway_manager.process_sicf_file("sicf.yml")

# load service instances
gateway_manager.load_service_instance(
    'sagr=3.spack=NNO-PASS0001.rsl-fg=1.raf=onlt1')
gateway_manager.load_service_instance(
    'sagr=3.spack=NNO-PASS0001.rsl-fg=1.rcf=onlt1', gvcid=(3, 0, 0))

# open service instances
gateway_manager.get_sle_server(
    'sagr=3.spack=NNO-PASS0001.rsl-fg=1.raf=onlt1').open()
gateway_manager.get_sle_server(
    'sagr=3.spack=NNO-PASS0001.rsl-fg=1.rcf=onlt1').open()


input("Enter to stop")


# close service instances
gateway_manager.get_sle_server(
    'sagr=3.spack=NNO-PASS0001.rsl-fg=1.raf=onlt1').close()
gateway_manager.get_sle_server(
    'sagr=3.spack=NNO-PASS0001.rsl-fg=1.rcf=onlt1').close()

# close the user side interface
user_interface.shutdown()
