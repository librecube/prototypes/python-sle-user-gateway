from sle_user_gateway import GatewayManager
from sle_user_gateway.user_interface.nis import CortexInterface


# define the user side interface
user_interface = CortexInterface()
user_interface.process_channel_file("channels.yml")
user_interface.startup()

# create gateway and read config and sicf files
gateway_manager = GatewayManager()
gateway_manager.load_user_interface(user_interface)
gateway_manager.process_proxy_config_file("proxy_config.yml")
gateway_manager.process_sicf_file("sicf.yml")

# load service instances
gateway_manager.load_service_instance(
    'sagr=1.spack=VST-PASS0001.rsl-fg=1.raf=onlt1')

gateway_manager.get_sle_server(
    'sagr=1.spack=VST-PASS0001.rsl-fg=1.raf=onlt1').open()

input("Enter to stop")

gateway_manager.get_sle_server(
    'sagr=1.spack=VST-PASS0001.rsl-fg=1.raf=onlt1').close()

# close the user side interface
user_interface.shutdown()
