import os

from sle_user_gateway.server import run


os.environ["SUG_USER_INTERFACE"] = "nis"
os.environ["SUG_INTERCOM_PORT"] = "7389"

run()
