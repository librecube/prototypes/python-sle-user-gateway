# Python SLE User Gateway

To be written...

## Installation

Clone the repository and then install via pip:

```
$ git clone https://gitlab.com/librecube/prototypes/python-sle-user-gateway
$ cd python-sle-user-gateway
$ virtualenv venv
$ . venv/bin/activate
$ pip install -e .
```

## Example

To be written...

## Documentation

The API documentation is in [docs/README.md](docs/README.md).

## Contribute

- Issue Tracker: https://gitlab.com/librecube/lib/python-sle/-/issues
- Source Code: https://gitlab.com/librecube/lib/python-sle

To learn more on how to successfully contribute please read the contributing
information in the [LibreCube guidelines](https://gitlab.com/librecube/guidelines).

## Support

If you are having issues, please let us know. Reach us at
[Matrix](https://app.element.io/#/room/#librecube.org:matrix.org)
or via [Email](mailto:info@librecube.org).

## License

The project is licensed under the MIT license. See the [LICENSE](./LICENSE.txt) file for details.
