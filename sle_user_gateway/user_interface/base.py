

class UserInterface:

    def __init__(self):
        self.gateway_manager = None

    def process_channel_file(self, file):
        """The channel file maps the data streams to ports."""
        raise NotImplementedError

    def startup(self):
        pass

    def shutdown(self):
        pass

    def parameter_indication(self, sle_server, pdu):
        raise NotImplementedError

    def status_report_indication(self, sle_server, pdu):
        raise NotImplementedError

    def frame_indication(self, sle_server, pdu):
        raise NotImplementedError
