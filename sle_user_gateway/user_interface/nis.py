import yaml
from nis_interface import AdminMessageInterfaceServer,\
    TelemetryMessageInterfaceServer,\
    TelemetryMessage, ccsds_time_to_datetime, CCSDS_MICROSECONDS

from ..sle_interface import RafInterface, RcfInterface
from .base import UserInterface


class Channel:
    CH_ADM_MSG = "CH_ADM_MSG"
    CH_TC = "CH_TC"
    CH_OCF = "CH_OCF"
    CH_TM_SLC = "CH_TM_SLC"  # RAF (all with good frame quality)
    CH_TM_MC = "CH_TM_MC"  # RCF (any VC)
    CH_TM_VC0 = "CH_TM_VC0"  # RCF VC0


class NisInterface(UserInterface):

    def __init__(self, host="0.0.0.0", version=1):
        self._host = host
        self._version = version
        self._user_servers = []

    def process_channel_file(self, filename):
        with open(filename, 'r') as f:
            document = yaml.safe_load(f)

        for entry in document:

            if entry['channel'] == Channel.CH_ADM_MSG:
                server = AdminMessageInterfaceServer(self._host, entry['port'])

            elif entry['channel'] == Channel.CH_TC:
                raise NotImplementedError

            elif entry['channel'] == Channel.CH_OCF:
                raise NotImplementedError

            else:  # all other channels are telemetry streams
                server = TelemetryMessageInterfaceServer(
                    self._host, entry['port'])

            user_server = {
                'channel': entry['channel'],
                'port': entry['port'],
                'delivery_mode': entry.get('delivery_mode'),
                'server': server,
            }
            self._user_servers.append(user_server)

    def startup(self):
        for user_server in self._user_servers:
            user_server['server'].bind()

    def shutdown(self):
        for user_server in self._user_servers:
            user_server['server'].unbind()

    def frame_indication(self, sle_server, pdu):
        # receives PDUs from all active SLE servers

        if isinstance(sle_server, RafInterface):
            # send through space link stream
            user_server = self._get_user_server(
                Channel.CH_TM_SLC, sle_server.delivery_mode)
            message = self._rsl_pdu_to_nis_message(pdu)
            user_server['server'].request(message)

        if isinstance(sle_server, RcfInterface):
            sc_id = sle_server.gvcid[0]
            vc_id = sle_server.gvcid[2]

            # send through space link stream
            user_server = self._get_user_server(
                Channel.CH_TM_SLC, sle_server.delivery_mode)
            message = self._rsl_pdu_to_nis_message(pdu)
            message.vc_id = vc_id
            message.sc_id = sc_id
            user_server['server'].request(message)

            # send through master channel stream
            user_server = self._get_user_server(
                Channel.CH_TM_MC, sle_server.delivery_mode)
            message = self._rsl_pdu_to_nis_message(pdu)
            message.vc_id = vc_id
            message.sc_id = sc_id
            user_server['server'].request(message)

            # send through virtual channel stream
            user_server = self._get_user_server(
                "CH_TM_VC{}".format(vc_id), sle_server.delivery_mode)
            message = self._rsl_pdu_to_nis_message(pdu)
            message.vc_id = vc_id
            message.sc_id = sc_id
            user_server['server'].request(message)

    def _get_user_server(self, channel, delivery_mode):
        for user_server in self._user_servers:
            if user_server['channel'] == channel and\
                    user_server['delivery_mode'] == delivery_mode:
                return user_server

    def _rsl_pdu_to_nis_message(self, pdu):
        ert_format = pdu['earthReceiveTime'].getName()
        ert = pdu['earthReceiveTime'][ert_format].prettyPrint()[2:]
        ert = bytes.fromhex(ert)
        time = ccsds_time_to_datetime(ert, CCSDS_MICROSECONDS)
        data = bytes.fromhex(pdu['data'].prettyPrint()[2:])

        return TelemetryMessage(
            self._version,
            time,
            data,
        )
