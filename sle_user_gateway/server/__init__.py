import logging; logging.basicConfig(level=logging.DEBUG)
import os
import time

from intercom import IntercomServer

from sle_user_gateway import GatewayManager


def run():
    INTERFACE = os.getenv('SUG_USER_INTERFACE')
    INTERCOM_PORT = int(os.getenv('SUG_INTERCOM_PORT'))

    if INTERFACE == 'zmq':
        raise NotImplementedError
    elif INTERFACE == 'nis':
        from sle_user_gateway.user_interface.nis import NisInterface
        user_interface = NisInterface(version=0)
    else:
        raise NotImplementedError

    if not os.path.exists("files/channels.yml"):
        print("File [files/channels.yml] not found.")

    if not os.path.exists("files/proxy_config.yml"):
        print("File [files/proxy_config.yml] not found.")

    print("Starting user interface...", end="")
    user_interface.process_channel_file("files/channels.yml")
    user_interface.startup()
    print("done")

    print("Starting gateway manager...", end="")
    gateway_manager = GatewayManager()
    gateway_manager.load_user_interface(user_interface)
    gateway_manager.process_proxy_config_file("files/proxy_config.yml")
    print("done")

    print("Starting intercom...", end="")
    intercom = IntercomServer('localhost', INTERCOM_PORT)
    intercom.register_command(
        'process_sicf_file', gateway_manager.process_sicf_file)
    intercom.register_command(
        'load_service_instance', gateway_manager.load_service_instance)
    intercom.register_command(
        'open_service_instance', gateway_manager.open_service_instance)
    intercom.register_command(
        'close_service_instance', gateway_manager.close_service_instance)
    intercom.start()
    print("done")

    print("SLE User Gateway Server is now running")
    print("Hit <Ctrl-C> to stop the server")
    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        pass

    print("Stopping the server")
    intercom.stop()
    user_interface.shutdown()


if __name__ == "__main__":
    run()
