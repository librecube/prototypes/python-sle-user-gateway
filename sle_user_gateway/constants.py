

class ServiceInstanceStatus:
    AVAILABLE = 'available'
    LOADED = 'loaded'


class ServiceType:
    RAF = 'RAF'
    RCF = 'RCF'
    FCLTU = 'FCLTU'


class DeliveryMode:
    ONLINE_TIMELY = 'ONLINE_TIMELY'
    ONLINE_COMPLETE = 'ONLINE_COMPLETE'
    OFFLINE = 'OFFLINE'


class InputMode:
    PROCESS = 'PROCESS'
    INPUT = 'INPUT'
    DATAFLOW = 'DATAFLOW'
