import time

import sle

from ..constants import ServiceInstanceStatus


class SleInterface:

    def __init__(self, *args, **kwargs):
        raise NotImplementedError

    def load(self):
        raise NotImplementedError

    def unload(self):
        if self.status == ServiceInstanceStatus.LOADED:
            if self._service.state == sle.State.UNBOUND:
                del self._service
                self.status = ServiceInstanceStatus.AVAILABLE

    def bind(self):
        if self.status == ServiceInstanceStatus.LOADED:
            self._service.bind()

    def unbind(self):
        if self.status == ServiceInstanceStatus.LOADED:
            self._service.unbind()

    def start(self):
        raise NotImplementedError

    def stop(self):
        if self.status == ServiceInstanceStatus.LOADED:
            self._service.stop()

    def open(self):
        self.load()
        self.bind()
        while not self._service.state == sle.State.BOUND:
            time.sleep(1)
        self.start()
        while not self._service.state == sle.State.ACTIVE:
            time.sleep(1)

    def close(self):
        self.stop()
        while not self._service.state == sle.State.BOUND:
            time.sleep(1)
        self.unbind()
        while not self._service.state == sle.State.UNBOUND:
            time.sleep(1)
        self.unload()

    def schedule_status_report(self, report_type='immediately', cycle=None):
        raise NotImplementedError

    def get_parameter(self, parameter):
        raise NotImplementedError

    def peer_abort(self, reason='otherReason'):
        raise NotImplementedError

    def change_input_mode(self):
        raise NotImplementedError

    def shutdown(self):
        raise NotImplementedError

    def _parameter_indication(self, pdu):
        self._user_interface.parameter_indication(self, pdu)

    def _status_report_indication(self, pdu):
        self._user_interface.status_report_indication(self, pdu)

    def _frame_indication(self, pdu):
        self._user_interface.frame_indication(self, pdu)
