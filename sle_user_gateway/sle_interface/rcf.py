import sle

from ..constants import ServiceInstanceStatus
from .base import SleInterface


class RcfInterface(SleInterface):

    def __init__(
            self,
            service_instance_id,
            responder_host,
            responder_port,
            auth_level,
            local_identifier,
            peer_identifier,
            local_password,
            peer_password,
            version_number,
            delivery_mode,
            gvcid,
            user_interface,
            ):
        self._service_instance_id = service_instance_id
        self._responder_host = responder_host
        self._responder_port = responder_port
        self._auth_level = auth_level
        self._local_identifier = local_identifier
        self._peer_identifier = peer_identifier
        self._local_password = local_password
        self._peer_password = peer_password
        self._version_number = version_number
        self._user_interface = user_interface
        self.delivery_mode = delivery_mode
        self.gvcid = gvcid

        self.status = ServiceInstanceStatus.AVAILABLE

    def load(self):
        if self.status == ServiceInstanceStatus.AVAILABLE:
            self._service = sle.RcfServiceUser(
                service_instance_identifier=self._service_instance_id,
                responder_host=self._responder_host,
                responder_port=self._responder_port,
                auth_level=self._auth_level,
                local_identifier=self._local_identifier,
                peer_identifier=self._peer_identifier,
                local_password=self._local_password,
                peer_password=self._peer_password,
                version_number=self._version_number,
                )
            self._service.parameter_indication = self._parameter_indication
            self._service.status_report_indication =\
                self._status_report_indication
            self._service.frame_indication = self._frame_indication

            self.status = ServiceInstanceStatus.LOADED

    def start(self):
        if self.status == ServiceInstanceStatus.LOADED:
            self._service.start(self.gvcid)
