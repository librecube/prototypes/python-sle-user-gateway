import yaml

from .constants import ServiceType, DeliveryMode
from .sle_interface import RafInterface, RcfInterface


class GatewayManager:

    def __init__(self):
        self._user_interface = None
        self._service_instances = {}
        self._sle_servers = {}

    def load_user_interface(self, user_interface):
        self._user_interface = user_interface

    def process_proxy_config_file(self, filename):
        with open(filename, 'r') as f:
            document = yaml.safe_load(f)
        self._local_identifier = document['LOCAL_ID']
        self._local_password = document['LOCAL_PASSWORD']
        self._proxy_config = document

    def process_sicf_file(self, filename):
        with open(filename, 'r') as f:
            document = yaml.safe_load(f)
        for entry in document:
            if entry['initiator-id'] != self._local_identifier:
                continue  # silently ignore this sicf file
            siid = entry['service-instance-id']
            self._service_instances[siid] = entry

    def load_service_instance(self, siid, input_mode=None, gvcid=None):
        si = self._service_instances.get(siid)
        service_type = self._get_service_type(siid)

        if service_type == ServiceType.RAF:
            host, port = self._get_responder_address(si['responder-port-id'])
            version = self._get_version(ServiceType.RAF)
            delivery_mode = self._get_delivery_mode(siid)

            server = RafInterface(
                service_instance_id=si['service-instance-id'],
                responder_host=host,
                responder_port=port,
                auth_level=self._get_auth_level(si['responder-id']),
                local_identifier=self._local_identifier,
                peer_identifier=si['responder-id'],
                local_password=self._local_password,
                peer_password=self._get_peer_password(si['responder-id']),
                version_number=version,
                delivery_mode=delivery_mode,
                user_interface=self._user_interface,
            )
        elif service_type == ServiceType.RCF:
            host, port = self._get_responder_address(si['responder-port-id'])
            version = self._get_version(ServiceType.RAF)
            delivery_mode = self._get_delivery_mode(siid)
            if gvcid not in self._get_permitted_vcids(siid):
                raise ValueError

            server = RcfInterface(
                service_instance_id=si['service-instance-id'],
                responder_host=host,
                responder_port=port,
                auth_level=self._get_auth_level(si['responder-id']),
                local_identifier=self._local_identifier,
                peer_identifier=si['responder-id'],
                local_password=self._local_password,
                peer_password=self._get_peer_password(si['responder-id']),
                version_number=version,
                delivery_mode=delivery_mode,
                gvcid=gvcid,
                user_interface=self._user_interface,
            )
        else:
            raise NotImplementedError

        self._sle_servers[siid] = server

    def open_service_instance(self, service_instance):
        self._get_sle_server(service_instance).open()

    def close_service_instance(self, service_instance):
        self._get_sle_server(service_instance).close()

    def _get_sle_server(self, siid):
        return self._sle_servers[siid]

    def _get_auth_level(self, peer):
        for entry in self._proxy_config['REMOTE_PEERS']:
            if peer == entry['ID']:
                return entry['AUTHENTICATION_MODE'].lower()

    def _get_peer_password(self, peer):
        for entry in self._proxy_config['REMOTE_PEERS']:
            if peer == entry['ID']:
                return entry['PASSWORD']

    def _get_responder_address(self, foreign_logical_port):
        for entry in self._proxy_config['FOREIGN_LOGICAL_PORTS']:
            if foreign_logical_port == entry['PORT_NAME']:
                host, port = entry['IP_ADDRESS'][0].split(":")
                port = int(port)
                return host, port

    def _get_version(self, server_type):
        for entry in self._proxy_config['SERVER_TYPES']:
            if server_type == entry['SRV_ID'].upper():
                return max(entry['SRV_VERSION'])

    def _get_service_type(self, siid):
        service_type = siid.split('.')[-1].split('=')[0]
        if service_type.upper() == 'RAF':
            return ServiceType.RAF
        elif service_type.upper() in ['RCF', 'VCF']:
            return ServiceType.RCF
        else:
            raise NotImplementedError

    def _get_delivery_mode(self, siid):
        delivery_mode = siid.split('.')[-1].split('=')[1]
        if delivery_mode.lower().startswith('onlc'):
            return DeliveryMode.ONLINE_COMPLETE
        elif delivery_mode.lower().startswith('onlt'):
            return DeliveryMode.ONLINE_TIMELY
        elif delivery_mode.lower().startswith('offl'):
            return DeliveryMode.OFFLINE
        else:
            raise ValueError

    def _get_permitted_vcids(self, siid):
        permitted_vcids = []
        si = self._service_instances.get(siid)
        if si:
            vcids = si['permitted-vcids'].split(".")
            for vcid in vcids:
                vcid = tuple([int(x) for x in vcid[1:-1].split(',')])
                permitted_vcids.append(vcid)
        return permitted_vcids

    def get_service_instance_status(self, siid):
        # TODO...
        service_type = self._get_service_type(siid)
        if service_type in [ServiceType.RAF, ServiceType.RCF]:
            return {
                "service-instance-id": siid,
                "service-type": 0,
                "state": 0,
                "substate": 0,
                "sle-provider": 0,
                "sle-port": 0,
                "sle-version": 0,
                "production-status": 0,
                "user-port": 0,
                "delivery-mode": 0,
                "input-mode": 0,
                "priority-level": 0,
                "data_quality": 0,
                "data-rate": 0,
                "received-data-units": 0,
                "connected-users": 0,
            }
        elif service_type == ServiceType.FCLTU:
            return {
                "service-instance-id": siid,
                "service-type": 0,
                "state": 0,
                "substate": 0,
                "sle_provider": 0,
                "production-status": 0,
                "connected-users": 0,
                "sent-data-units": 0,
            }
