from intercom import IntercomClient
import click


client = IntercomClient('localhost', 7389)


@click.group()
def cli():
    pass


@cli.command()
@click.argument('file')
def process_sicf_file(file):
    client.send('process_sicf_file', file)


@cli.command()
@click.argument('service_instance')
def load_service_instance(service_instance):
    client.send('load_service_instance', service_instance)


@cli.command()
@click.argument('service_instance')
def open_service_instance(service_instance):
    client.send('open_service_instance', service_instance)


@cli.command()
@click.argument('service_instance')
def close_service_instance(service_instance):
    client.send('close_service_instance', service_instance)


if __name__ == "__main__":
    cli()
