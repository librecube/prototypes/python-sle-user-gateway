from setuptools import setup, find_packages

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setup(
    name='sle-user-gateway',
    version='0.0.1',
    author_email="info@librecube.org",
    description='SLE User Gateway',
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/librecube/prototypes/python-sle-user-gateway",
    license='GPL',
    python_requires='>=3.6',
    packages=find_packages(exclude=['examples', 'tests']),
    install_requires=['sle', 'pyyaml'],
    extras_require={
        "cli": ["click"],
    },
    entry_points={
        'console_scripts': [
            'sug-server = sle_user_gateway.server:run',
            'sug-cli = sle_user_gateway.clients.cli:cli',
        ],
    }
)
